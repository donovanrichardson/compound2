package state;

public class Compound {

    final String id;

    public Compound(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
