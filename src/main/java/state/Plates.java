package state;

import java.util.*;

public class Plates {

    Map<String, Compound> comps;
    Map<String, Compound> wells;
//    Scanner input;


    public Plates(/*Scanner s*/) {
        this.comps = new HashMap<>();
        this.wells = new HashMap<>();
//        this.input = s;

    }

    public boolean addCompound(String id) {
        comps.put(id, new Compound(id));
        return true;
    }

    public void addToWell(String well, String id) {
        Compound c = comps.get(id);
        if (c == null){
            throw new NullPointerException(String.format("Compound %s does not exist", id));
        }
        if(wells.containsKey(well)){
            throw new IllegalStateException(String.format("Well %s already contains a compound", well));
        }
        wells.put(well, c);
    }

    public void wellTransfer(String origin, String... destinations) {
        Compound c = wells.get(origin);

        if(!wells.containsKey(origin)){
            throw new IllegalStateException(String.format("There are no contents in well %s, or it does not exist", origin));
        }

        for(String dest : destinations){
            if(wells.get(dest) != null){
                throw new IllegalStateException(String.format("Well %s contents cannot be transferred. One or more destination wells already contains a compound", origin));
            }
        }

        for(String dest : destinations){
            wells.put(dest, c);
        }

    }

    public String wellInfo(String well) {
        try{
            return wells.get(well).getId();
        } catch (NullPointerException n){
            return "";
        }
    }

    public boolean existsCompound(String id){
        return this.comps.containsKey(id);
    }
}
