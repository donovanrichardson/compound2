import state.Plates;

import java.util.Scanner;

public class CompoundTrack {



    public static void main(String[] args) {
        Plates p = new Plates();
        Scanner s = new Scanner(System.in);

        while(true){
            System.out.println("Enter N to add a new compound");
            // It is not possible to a compound into a well in which there already exists a compound.
            System.out.println("Enter W to put a compound into a well");
            // below I assume that the first well is not completely emptied by this process
            System.out.println("Enter T to transfer the contents one well to another well.");
            System.out.println("Enter I to find what compound, if any, is in a well.");
            System.out.println("Enter Q to quit.");

            String line = s.nextLine();
            String lowerLine = line.toLowerCase();

            if (lowerLine.equals("n")){
                System.out.println("Enter the ID of the new compound.");
                String id = s.nextLine();
                if(p.addCompound(id)){
                    System.out.println(String.format("Compound %s added.", id));
                }
            }else if (lowerLine.equals("w")){

                while(true){
                    System.out.println("Enter the ID of the compound you want to add.");
                    String id = s.nextLine();
                    if(!p.existsCompound(id)){
                        System.out.println(String.format("Compound %s has not yet been added to the inventory.", id));
                        break;
                    }else {

                        System.out.println("Enter the ID of the well to which you want to add the compound.");
                        String well = s.next();
                        String extra = s.nextLine();
                        p.addToWell(well, id);
                        System.out.println(String.format("Compound %s added to well %s", id, well));
                        break;
                    }
                }

            }else if (lowerLine.equals("t")){
                System.out.println("Enter the ID of the well you want to transfer from.");
                String origin = s.next();
                String extra2 = s.nextLine();
                System.out.println("Enter the ID of the well(s) you want to transfer to, separated by spaces.");
                String dest = s.nextLine();
                String[] dests = dest.split(" ");
                try{
                    p.wellTransfer(origin, dests);
                } catch (IllegalStateException e){
                    System.out.println(e.getMessage());
                    continue;
                }
            }else if (lowerLine.equals("i")){
                System.out.println("Enter the ID of the well whose contents you want to know.");
                String well = s.next();
                String extra3 = s.nextLine();
                System.out.println(String.format("Compound in this well: %s",p.wellInfo(well)));

            } else if (lowerLine.equals("q")){
                System.out.println("goodbye");
                s.close();
                break;
        }else System.out.println(String.format("\"%s\" is not a valid input.", line));

        }


    }

//    private static void addCompound() {
//
//    }
//
//    private static void addToWell() {
//    }
//    private static void wellTransfer() {
//    }
//    private static void wellInfo() {
//    }
}