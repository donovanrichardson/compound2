import org.junit.Test;
import state.Plates;

import static org.junit.Assert.*;

public class PlatesTest {

    @Test
    public void addCompoundTest(){
        String id = "1234";
        Plates p = new Plates();
        assertFalse(p.existsCompound(id));
        p.addCompound(id);
        assertTrue(p.existsCompound(id));
    }

    @Test
    public void addToWellTest(){
        String id = "1234";
        Plates p = new Plates();
        p.addCompound("1234");

        try{
            p.addToWell("well", "5678");
            fail();
        }catch(NullPointerException n){
            assertEquals("Compound 5678 does not exist", n.getMessage());
        }catch(Exception n){
            fail();
        }

        p.addToWell("well", id);

        assertEquals(id, p.wellInfo("well"));

        String c2 = "5678";
        p.addCompound(c2);

        try{
            p.addToWell("well", c2);
        } catch(IllegalStateException s){
            assertEquals("Well well already contains a compound", s.getMessage());
        }

        p.addToWell("A", c2);

        try{
            p.wellTransfer("well", "A");
        } catch(IllegalStateException s){
            assertEquals("Well well contents cannot be transferred. One or more destination wells already contains a compound", s.getMessage());
        }

        p.wellTransfer("well", "well2", "well3");

        try{
            p.wellTransfer("well3", "well", "well2");
        } catch(IllegalStateException s){
            assertEquals("Well well3 contents cannot be transferred. One or more destination wells already contains a compound", s.getMessage());
        }

        try{
            p.wellTransfer("A", "well2");
        } catch(IllegalStateException s){
            assertEquals("Well A contents cannot be transferred. One or more destination wells already contains a compound", s.getMessage());
        }

        try{
            p.wellTransfer("Q", "well5");
        } catch(IllegalStateException s){
            assertEquals("There are no contents in well Q, or it does not exist", s.getMessage());
        }

        p.wellTransfer("A", "B");

        assertEquals(id ,p.wellInfo("well"));
        assertEquals(id ,p.wellInfo("well2"));
        assertEquals(id ,p.wellInfo("well3"));
        assertEquals(c2 ,p.wellInfo("A"));
        assertEquals(c2 ,p.wellInfo("B"));
        assertEquals("" ,p.wellInfo("C"));


    }

}
