# Compound #

Compound is a command line tool to record the compounds to added to wells on a plate.

With this tool, you can:
* Add compounds to your inventory
* Add these compounds to a well
* Query the contents of a well
* Transfer contents from one well to other wells.

In order to run this on your computer:

```
git clone https://gitlab.com/donovanrichardson/compound2.git
cd compound
```

to run tests:

```
mvn clean test
```

to run tests and build:
```
mvn clean install
```

to run:
```
java -jar target/compound-1.0-SNAPSHOT.jar

```

you can execute these three steps at once using `mvn exec:java`, but the jar in the target folder will remain unchanged.